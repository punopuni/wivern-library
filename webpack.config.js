const fs = require('fs');
let path = require('path');
const HtmlWebpackPlugin = require('html-webpack-plugin');



const pugPath = path.resolve(__dirname, './app/static/pug/');
const stylPath = path.resolve(__dirname, './app/static/stylus/');

const pages =
	fs
	.readdirSync(pugPath)
	.filter(fileName => !fileName.search('[^_]*.pug'))
// Сея операция очень небезопасна но я надеюсь на вашу сознательность
// !filename.search('[^_]*.pug')

// Вспомогательная функция
function pugNameToHtml(page) {
	let result = page.substr(0, page.length - 3);
	result += 'html';
	return result;
}

let conf = {
	entry: './app/static/js/index.js',
	watch: true,
	output: {
		path: path.resolve(__dirname, './public_html/js/'),
		filename: 'all.min.js'
	},
	module: {
		rules: [{
			test: /\.js$/,
			loader: 'babel-loader',
		}]
	}
};

module.exports = conf;
