/**
 * showDropdown - функция добавляет класс для открытия dropdown
 * resetDropdowns - функция для закрытия dropdown
 * documentHandler - функция при клике по окну звызывает resetDropdowns и закрывает его
 *
 *
 */
var elements = document.querySelectorAll(".dropdown-toggle");
if (elements) {
  function showDropdown(event){
    event.target.parentNode.classList.add("show");
  }

  function resetDropdowns(){
    var element = document.querySelector(".dropdown.show");
    if(element){ element.classList.remove("show"); }
  }

  function documentHandler(){
    resetDropdowns();
    document.removeEventListener('click', documentHandler , false);
  };

  if(elements.length>0){
    for (var i = 0;  i < elements.length; i++ ) {
      elements[i].addEventListener('click',function(e){
        var isActive = e.target.parentNode.classList.contains("show");
        resetDropdowns();
        if (isActive) {
          return;
        }
        e.stopPropagation();
        e.preventDefault();
        showDropdown(e);
        document.addEventListener('click', documentHandler, false); //add document click listener to close dropdown on outside click
      }, false);
    }
  }
}
