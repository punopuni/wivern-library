/**
 * toggleAccordion - функция аккордиончика
 * showText - функция берет высоту элемента, для правильного открытия и закрытия
 * item.classList.remove('accordion--show') - позволяет открывать только один элемент единовременно.
 */
const accordion = document.querySelector('.accordion');
if (accordion) {
  const items     = accordion.querySelectorAll('.accordion__item');
  const accordionHeader = accordion.querySelectorAll('.accordion__header');
  //Lets figure out what item to click
  function toggleAccordion() {
    const thisItem = this.parentNode;

    for (let i = 0; i < items.length; i++) {
      let item = items[i];
      if (thisItem == item) {
        showText(thisItem);
        thisItem.classList.toggle('accordion--show');
        continue;
      }
      item.classList.remove('accordion--show');
    }
  }
  for (let i = 0; i < accordionHeader.length; i++) {
    accordionHeader[i].addEventListener('click', toggleAccordion);
  }
  function showText(textEl) {
      textEl.querySelector('.accordion--collapse').style.height = textEl.querySelector('.accordion--collapse').scrollHeight + 'px';
    }
}