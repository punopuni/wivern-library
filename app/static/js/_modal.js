/**
 * Скачиваем Vue-js-modal - npm install vue-js-modal --save
 * Подключаем Vue и Vue-js-modal import-ами
 * Вызываем наш плагин Vue.use(VModal);
 * Создаем новый элемент Vue и используем методы show и hide для открытия и закрытия при клике
 *
 */
import Vue from 'vue/dist/vue.js'
import VModal from 'vue-js-modal'

Vue.use(VModal);

(function () {
    if(!document.getElementById("modal")) return;

    new Vue({
      el: '#modal',
      components: {
      },
      methods: {
        show () {
          this.$modal.show('modal');
        },
        hide () {
          this.$modal.hide('modal');
        }
      }
    });
})();
